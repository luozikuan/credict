# -*- coding: utf-8 -*-
#!/usr/bin/python

import httplib, urllib
import json

def lookup(word):
    for char in word:
        if char >= u'\u4e00' and char <= u'\u9fa5':
            f, t = 'zh', 'en'
        else:
            f, t = 'en', 'zh'
    api_key = 'tqdC7fTLxsiFKueG2jjqCFmj'
    params = urllib.urlencode({
        'client_id' : api_key,
        'q' : word.encode('utf8'),
        'from' : f,
        'to' : t
    })
    conn = httplib.HTTPConnection("openapi.baidu.com")
    conn.request('GET', '/public/2.0/translate/dict/simple?%s' % params)
    result = conn.getresponse().read()
    conn.close()
    return result

def parse(meaning):
    m = json.loads(meaning)
    if m['errno'] != 0:
        return 'query error'
    if 'symbols' not in m['data']:
        return 'no result'
    result = ''
    for sym in m['data']['symbols']:
        if m['from'] == 'en' and m['to'] == 'zh':
            result += '*[%s], [%s]\n\n' % (sym['ph_am'], sym['ph_en'])
        else:
            result += '*[%s]\n\n' % sym['ph_zh']
        for part in sym['parts']:
            result += part['part']
            if not part['part'].endswith('.'):
                result += '.'
            result += ' '
            if 'means' in part:
                for mean in part['means']:
                    result += '%s; ' % mean
            result += '\n'
    return result

if __name__ == '__main__':
    while True:
        word = raw_input(">> ")
        if len(word) == 0:
            continue
        result = lookup(word.decode('utf8'))
        print parse(result)
